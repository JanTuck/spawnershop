/*
 * Copyright (c) 2017 Jan Tuck.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software/plugin and associated documentation files (the "Software/Plugin"), to deal in the Software/Plugin without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software/Plugin.
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software/Plugin.
 *
 * THE SOFTWARE/PLUGIN IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE/PLUGIN OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package me.jantuck.spawnshop.mobtypes;

import me.jantuck.spawnshop.configmanager.ConfigManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

/**
 * Created by Jan on 27-01-2017.
 */
public enum MobType {
    ZOMBIE_PIGMAN("Zombie Pigman", 57, EntityType.PIG_ZOMBIE),
    IRON_GOLEM("Iron Golem", 95, EntityType.IRON_GOLEM),
    POLAR_BEAR("Polar Bear", 102, EntityType.POLAR_BEAR),
    ZOMBIE_HORSE("Zombie Horse", 54, EntityType.ZOMBIE_HORSE),
    WITHER_SKELETON("Wither Skeleton", 58, EntityType.WITHER_SKELETON),
    SKELETON_HORSE("Skeleton Horse", 28, EntityType.SKELETON_HORSE),
    BLAZE("Blaze", 61, EntityType.BLAZE),
    CAVE_SPIDER("Cave Spider", 59, EntityType.CAVE_SPIDER),
    CREEPER("Creeper", 50, EntityType.CREEPER),
    ENDERMAN("Enderman", 58, EntityType.ENDERMAN),
    Ghast("Ghast", 56, EntityType.GHAST),
    MAGMA_CUBE("Magma cube", 62, EntityType.MAGMA_CUBE),
    SILVERFISH("SilverFish", 60, EntityType.SILVERFISH),
    SKELETON("Skeleton", 51, EntityType.SKELETON),
    SLIME("Slime", 55, EntityType.SLIME),
    SPIDER("Spider", 52, EntityType.SPIDER),
    WITCH("Witch", 56, EntityType.WITCH),
    ZOMBIE("Zombie", 54, EntityType.ZOMBIE),
    BAT("Bat", 65, EntityType.BAT),
    CHICKEN("Chicken", 93, EntityType.CHICKEN),
    COW("Cow", 92, EntityType.COW),
    HORSE("Horse", 100, EntityType.HORSE),
    MOOSHROOM("Mooshroom", 96, EntityType.MUSHROOM_COW),
    OCELOT("Ocelot", 98, EntityType.OCELOT),
    PIG("Pig", 90, EntityType.PIG),
    SHEEP("Sheep", 91, EntityType.SHEEP),
    SQUID("Squid", 94, EntityType.SQUID),
    WOLF("Wolf", 95, EntityType.WOLF),
    VILLAGER("Villager", 120, EntityType.VILLAGER),
    SNOWMAN("Snowman", 97, EntityType.SNOWMAN),
    DONKEY("Donkey", 100, EntityType.DONKEY),
    LLAMA("Llama", 100, EntityType.LLAMA),
    RABBIT("Rabbit", 101, EntityType.RABBIT),
    GUARDIAN("Guardian", 68, EntityType.GUARDIAN),
    VINDICATOR("Vindicator", 4, EntityType.VINDICATOR),
    MULE("Mule", 100, EntityType.MULE),
    ENDERMITE("Endermite", 67, EntityType.ENDERMITE);

    private String name;
    private int price;
    private ItemStack spawner;
    private ItemStack spawnEgg;
    private EntityType entityType;

    MobType(String name, int eggID, EntityType entityType) {
        this.name = name;
        this.entityType = entityType;
        this.price = ConfigManager.getConfigManager().getPrice(name);
        this.spawner = new ItemStack(Material.MOB_SPAWNER, 1);
        ItemMeta itemMeta = spawner.getItemMeta();
        itemMeta.setDisplayName(ConfigManager.SPAWNER_PREFIX + ConfigManager.getConfigManager().getSpawnerName(name).replace("%type%", name));
        itemMeta.setLore(ConfigManager.getConfigManager().getSpawnerLore(name));
        this.spawner.setItemMeta(itemMeta);
        this.spawnEgg = new ItemStack(Material.MONSTER_EGG, 1, (short) eggID);
        this.spawnEgg.setItemMeta(itemMeta);
        itemMeta = spawnEgg.getItemMeta();
        itemMeta.setDisplayName(ConfigManager.SPAWNER_PREFIX + ConfigManager.getConfigManager().getSpawnerName(name).replace("%type%", name));
        itemMeta.setLore(Arrays.asList(ChatColor.GOLD + "Price: " + this.price));
        spawnEgg.setItemMeta(itemMeta);
    }

    public static MobType[] getAllMobTypes() {
        return MobType.BLAZE.getDeclaringClass().getEnumConstants();
    }

    public static MobType matchMobType(String name) {
        for (MobType mobType : MobType.getAllMobTypes()) {
            if (name.toLowerCase().contains(mobType.getName().toLowerCase().replace(" ", ""))) {
                return mobType;
            }
            if (name.toLowerCase().contains(mobType.getName().toLowerCase())) {
                return mobType;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public ItemStack getSpawner() {
        return spawner.clone();
    }

    public void setSpawner(Block block) {
        block.setType(Material.MOB_SPAWNER);
        BlockState blockState = block.getState();
        CreatureSpawner spawner = ((CreatureSpawner) blockState);
        spawner.setSpawnedType(getEntityType());
        blockState.update();
    }

    public ItemStack getSpawnEgg() {
        return spawnEgg.clone();
    }

    public void updateLoreAndPrice() {
        this.price = ConfigManager.getConfigManager().getPrice(name);
        ItemMeta itemMeta = spawnEgg.getItemMeta();
        itemMeta.setDisplayName(ConfigManager.SPAWNER_PREFIX + ConfigManager.getConfigManager().getSpawnerName(name).replace("%type%", name));
        itemMeta.setLore(Arrays.asList(ChatColor.GOLD + "Price: " + this.price));
        spawnEgg.setItemMeta(itemMeta);
        itemMeta = spawner.getItemMeta();
        itemMeta.setDisplayName(ConfigManager.SPAWNER_PREFIX + ConfigManager.getConfigManager().getSpawnerName(name).replace("%type%", name));
        itemMeta.setLore(ConfigManager.getConfigManager().getSpawnerLore(name));
        this.spawner.setItemMeta(itemMeta);
    }

    public boolean isEnabled() {
        return ConfigManager.getConfigManager().isEnabled(name);
    }


}
