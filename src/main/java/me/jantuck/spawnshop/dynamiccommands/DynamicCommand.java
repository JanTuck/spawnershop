/*
 * Copyright (c) 2017 Jan Tuck.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software/plugin and associated documentation files (the "Software/Plugin"), to deal in the Software/Plugin without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software/Plugin.
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software/Plugin.
 *
 * THE SOFTWARE/PLUGIN IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE/PLUGIN OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package me.jantuck.spawnshop.dynamiccommands;


import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by janx3783 on 23-01-2017.
 */
public abstract class DynamicCommand {
    protected String description;
    protected String usageMessage;
    private String name;
    private List<String> aliases = new ArrayList<>();
    private String permission;
    private String permissionMessage;
    private DynamicCommand dynamicCommand;

    public DynamicCommand(String name, String description, String usageMessage) {
        this(name, description, usageMessage, null, null, null);
    }

    public DynamicCommand(String name, String description, String usageMessage, List<String> aliases) {
        this(name, description, usageMessage, aliases, null, null);
    }

    public DynamicCommand(String name, String description, String usageMessage, List<String> aliases, String permission) {

        this(name, description, usageMessage, aliases, permission, null);
    }

    public DynamicCommand(String name, String description, String usageMessage, String permission) {
        this(name, description, usageMessage, null, permission, null);
    }

    public DynamicCommand(String name, String description, String usageMessage, String permission, String permissionMessage) {
        this(name, description, usageMessage, null, permission, permissionMessage);
    }

    public DynamicCommand(String name, String description, String usageMessage, List<String> aliases, String permission, String permissionMessage) {
        this.name = name;
        this.description = description;
        this.usageMessage = usageMessage;
        this.aliases = aliases;
        this.permission = permission;
        this.permissionMessage = permissionMessage;
        dynamicCommand = this;
        this.registerCommand(name);
    }

    private void registerCommand(String name) {
        if (aliases == null)
            aliases = new ArrayList<>();
        Command command = new Command(name, description, usageMessage, aliases) {
            @Override
            public boolean execute(CommandSender commandSender, String s, String[] strings) {
                if (permission == null || commandSender.hasPermission(permission)) {
                    return dynamicCommand.execute(commandSender, this, s, strings);
                } else {
                    commandSender.sendMessage(permissionMessage);
                    return false;
                }
            }
        };
        command.setPermission(permission);
        command.setPermissionMessage(permissionMessage);
        DynamicCommandRegistering.getInstance().registerCommand(name, command);
    }

    public abstract boolean execute(CommandSender commandSender, Command command, String commandLabel, String[] args);
}
