/*
 * Copyright (c) 2017 Jan Tuck.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software/plugin and associated documentation files (the "Software/Plugin"), to deal in the Software/Plugin without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software/Plugin.
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software/Plugin.
 *
 * THE SOFTWARE/PLUGIN IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE/PLUGIN OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package me.jantuck.spawnshop.dynamiccommands;

import me.jantuck.spawnshop.reflections.ReflectionUtil;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.plugin.Plugin;

import java.util.List;


/**
 * Created by janx3783 on 23-01-2017.
 */
public class DynamicCommandRegistering {
    private static DynamicCommandRegistering instance;
    private CommandMap commandMap;
    private Plugin plugin;

    public DynamicCommandRegistering(Plugin plugin) {
        this.plugin = plugin;

        Server server = Bukkit.getServer();
        Class<?> serverClass = server.getClass();
        commandMap = ReflectionUtil.getPrivateFieldObject(serverClass, server, "commandMap");
        instance = this;
    }

    public static DynamicCommandRegistering getInstance() {
        return instance;
    }

    public void registerCommand(String name, Command command) {
        commandMap.register(name, command);
    }

    public void registerCommand(String name, List<Command> commands) {
        commandMap.registerAll(name, commands);
    }
}
