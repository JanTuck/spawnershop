/*
 * Copyright (c) 2017 Jan Tuck.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software/plugin and associated documentation files (the "Software/Plugin"), to deal in the Software/Plugin without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software/Plugin.
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software/Plugin.
 *
 * THE SOFTWARE/PLUGIN IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE/PLUGIN OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package me.jantuck.spawnshop;

import de.dustplanet.silkspawners.SilkSpawners;
import me.jantuck.spawnshop.commands.CommandInitializer;
import me.jantuck.spawnshop.configmanager.ConfigManager;
import me.jantuck.spawnshop.dynamiccommands.DynamicCommandRegistering;
import me.jantuck.spawnshop.events.BlockPlaceEvent;
import me.jantuck.spawnshop.events.InventoryEvents;
import me.jantuck.spawnshop.events.PlayerInteractEvent;
import me.jantuck.spawnshop.events.SignChangeEvent;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

/**
 * Created by Jan on 27-01-2017.
 */
public class SSCore extends JavaPlugin {

    public static Economy econ = null;
    private static SSCore ssCore;
    private SilkSpawners silkSpawners;

    public static SSCore getSSCore() {
        return ssCore;
    }

    @Override
    public void onLoad() {
        new DynamicCommandRegistering(this);
    }

    public boolean hasSilkSpawners() {
        return silkSpawners != null;
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    @Override
    public void onEnable() {

        new ConfigManager(this);
        if (!setupEconomy()) {
            getServer().getLogger().log(Level.WARNING, String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        ssCore = this;
        silkSpawners = SilkSpawnersProvider.getInstance();
        saveDefaultConfig();
        PluginManager pluginManager = getServer().getPluginManager();
        pluginManager.registerEvents(new InventoryEvents(), this);
        pluginManager.registerEvents(new BlockPlaceEvent(), this);
        pluginManager.registerEvents(new SignChangeEvent(), this);
        pluginManager.registerEvents(new PlayerInteractEvent(), this);
        new CommandInitializer().init();
    }

    @Override
    public void onDisable() {
        ssCore = null;
    }

}
