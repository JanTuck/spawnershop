/*
 * Copyright (c) 2017 Jan Tuck.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software/plugin and associated documentation files (the "Software/Plugin"), to deal in the Software/Plugin without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software/Plugin.
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software/Plugin.
 *
 * THE SOFTWARE/PLUGIN IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE/PLUGIN OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package me.jantuck.spawnshop.configmanager;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jan on 27-01-2017.
 */
public class ConfigManager {
    public static final String SPAWNER_PREFIX = ChatColor.translateAlternateColorCodes('&', "&4&l&b&3&o&n&k&m&r");
    private static ConfigManager configManager;
    private Plugin plugin;
    private FileConfiguration fileConfiguration;

    public ConfigManager(Plugin plugin) {
        this.plugin = plugin;
        configManager = this;
        fileConfiguration = plugin.getConfig();
    }

    public static ConfigManager getConfigManager() {
        return configManager;
    }

    public int getPrice(String name) {
        return fileConfiguration.getInt(name + ".price");
    }

    public boolean isEnabled(String name) {
        return fileConfiguration.getBoolean(name + ".enabled");
    }

    public String getBuyMessage() {
        return ChatColor.translateAlternateColorCodes('&', fileConfiguration.getString("BuyMessage"));
    }

    public String getNotEnoughItemsMessage() {
        return ChatColor.translateAlternateColorCodes('&', fileConfiguration.getString("NotEnoughItems"));
    }

    public String getSellSuccessMessage() {
        return ChatColor.translateAlternateColorCodes('&', fileConfiguration.getString("SellSuccess"));
    }

    public String getCommand() {
        return fileConfiguration.getString("Command");
    }

    public String getNotEnoughMoneyMessage() {
        return ChatColor.translateAlternateColorCodes('&', fileConfiguration.getString("NotEnoughMoney"));
    }

    public String getSellSignMessage() {
        return ChatColor.translateAlternateColorCodes('&', fileConfiguration.getString("SellSignMessage"));
    }

    public String getBuySignMessage() {
        return ChatColor.translateAlternateColorCodes('&', fileConfiguration.getString("BuySignMessage"));
    }

    public String getCreateBuySignString() {
        return ChatColor.translateAlternateColorCodes('&', fileConfiguration.getString("BuySignCreateString"));
    }

    public String getCreateSellSignString() {
        return ChatColor.translateAlternateColorCodes('&', fileConfiguration.getString("SellSignCreateString"));
    }

    public String getSpawnerName(String name) {
        return ChatColor.translateAlternateColorCodes('&', fileConfiguration.getString(name + ".name"));
    }

    public List<String> getSpawnerLore(String name) {
        List<String> stringList = fileConfiguration.getStringList(name + ".spawnerlore");

        List<String> tempList = new ArrayList<>();
        stringList.forEach(entry -> tempList.add(ChatColor.translateAlternateColorCodes('&', entry).replace("%type%", name)));
        return tempList;
    }

    public String getMoneyPrefix() {
        return fileConfiguration.getString("MoneyPrefix");
    }


    public void reloadConfig() {
        plugin.reloadConfig();
        this.fileConfiguration = plugin.getConfig();
    }
}
