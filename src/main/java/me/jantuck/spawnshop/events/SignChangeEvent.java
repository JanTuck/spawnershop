/*
 * Copyright (c) 2017 Jan Tuck.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software/plugin and associated documentation files (the "Software/Plugin"), to deal in the Software/Plugin without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software/Plugin.
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software/Plugin.
 *
 * THE SOFTWARE/PLUGIN IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE/PLUGIN OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package me.jantuck.spawnshop.events;

import me.jantuck.spawnshop.configmanager.ConfigManager;
import me.jantuck.spawnshop.mobtypes.MobType;
import me.jantuck.spawnshop.utils.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * Created by Jan on 28-01-2017.
 */
public class SignChangeEvent implements Listener {

    @EventHandler
    public void onSignChange(org.bukkit.event.block.SignChangeEvent event) {
        Player player = event.getPlayer();
        if (event.getLine(0).equalsIgnoreCase(ConfigManager.getConfigManager().getCreateBuySignString())) {
            if (!player.hasPermission("spawner.sign.create")) {
                event.setLine(0, ChatColor.DARK_RED + "NO PERMISSION");
                event.setLine(1, "");
                event.setLine(2, "");
                event.setLine(3, "");
                return;
            }
            event.setLine(0, ConfigManager.getConfigManager().getBuySignMessage());
            MobType mobType = MobType.matchMobType(event.getLine(2));
            if (!mobType.isEnabled()) {
                event.setLine(0, ChatColor.DARK_BLUE + "Invalid Mob Type");
                event.setLine(2, "Not Enabled!");
                return;
            }
            if (StringUtils.toInteger(event.getLine(1)) > 64) {
                event.setLine(0, "Invalid Sign!");
                event.setLine(1, ChatColor.DARK_RED + "INVALID AMOUNT");
                return;
            }
            if (mobType == null) {
                event.setLine(0, ChatColor.DARK_BLUE + "Invalid Mob Type");
                event.setLine(2, "UNKNOWN");
            } else {
                event.setLine(2, ChatColor.DARK_GREEN + mobType.getName());
            }

            String price = event.getLine(3).replaceAll("[^0-9]", "");
            event.setLine(3, ConfigManager.getConfigManager().getMoneyPrefix() + price);
        } else if (event.getLine(0).equalsIgnoreCase(ConfigManager.getConfigManager().getCreateSellSignString())) {
            if (!player.hasPermission("spawner.sign.create")) {
                event.setLine(0, ChatColor.DARK_RED + "NO PERMISSION");
                event.setCancelled(true);
                return;
            }
            event.setLine(0, ConfigManager.getConfigManager().getSellSignMessage());
            MobType mobType = MobType.matchMobType(event.getLine(2));
            if (!mobType.isEnabled()) {
                event.setLine(0, ChatColor.DARK_BLUE + "Invalid Mob Type");
                event.setLine(2, "Not Enabled!");
                return;
            }
            if (StringUtils.toInteger(event.getLine(1)) > 64) {
                event.setLine(0, "Invalid Sign!");
                event.setLine(1, ChatColor.DARK_RED + "INVALID AMOUNT");
                return;
            }
            if (mobType == null) {
                event.setLine(0, ChatColor.DARK_BLUE + "Invalid Mob Type");
                event.setLine(2, "UNKNOWN");
            } else {
                event.setLine(2, ChatColor.DARK_RED + mobType.getName());
            }
            String price = event.getLine(3).replaceAll("[^0-9]", "");
            event.setLine(3, ConfigManager.getConfigManager().getMoneyPrefix() + price);
        }
    }
}
