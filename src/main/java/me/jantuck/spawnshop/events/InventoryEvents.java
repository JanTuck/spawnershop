/*
 * Copyright (c) 2017 Jan Tuck.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software/plugin and associated documentation files (the "Software/Plugin"), to deal in the Software/Plugin without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software/Plugin.
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software/Plugin.
 *
 * THE SOFTWARE/PLUGIN IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE/PLUGIN OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package me.jantuck.spawnshop.events;

import me.jantuck.spawnshop.SSCore;
import me.jantuck.spawnshop.configmanager.ConfigManager;
import me.jantuck.spawnshop.mobtypes.MobType;
import me.jantuck.spawnshop.utils.StringUtils;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Created by Jan on 27-01-2017.
 */
public class InventoryEvents implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Inventory inventory = event.getInventory();
        String inventoryTitle = inventory.getTitle();
        Player player = (Player) event.getWhoClicked();
        if (inventoryTitle.equals(ChatColor.RED + "" + ChatColor.BOLD + "Spawner Shop")) {
            ItemStack currentItem = event.getCurrentItem();
            if (currentItem != null && currentItem.hasItemMeta() && currentItem.getItemMeta().hasDisplayName() && currentItem.getItemMeta().hasLore()) {
                String displayName = currentItem.getItemMeta().getDisplayName();
                List<String> lore = currentItem.getItemMeta().getLore();
                if (displayName.startsWith(ConfigManager.SPAWNER_PREFIX)) {
                    int price = StringUtils.toInteger(ChatColor.stripColor(lore.get(0).replace("Price: ", "")));
                    if (price > 0) {
                        String itemName = ChatColor.stripColor(displayName);
                        EconomyResponse withdrawel = SSCore.econ.withdrawPlayer(player, price);
                        if (withdrawel.transactionSuccess()) {
                            MobType mobType = MobType.matchMobType(itemName);
                            player.sendMessage(ConfigManager.getConfigManager().getBuyMessage().replace("%type%", mobType.getName()).replace("%money%", ConfigManager.getConfigManager().getMoneyPrefix() + price + "").replace("%amount%", "1"));
                            int firstEmpty = player.getInventory().firstEmpty();
                            if (firstEmpty == -1) {
                                player.getWorld().dropItem(player.getLocation().add(0, 0.5, 0), mobType.getSpawner());
                            } else {
                                player.getInventory().addItem(mobType.getSpawner());
                            }
                        } else {
                            player.sendMessage(ConfigManager.getConfigManager().getNotEnoughMoneyMessage().replace("%type%", itemName).replace("%money%", ConfigManager.getConfigManager().getMoneyPrefix() + price + ""));
                        }
                    }
                }
            }
            event.setCancelled(true);
        }
    }
}
