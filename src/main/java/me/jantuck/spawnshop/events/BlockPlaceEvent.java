/*
 * Copyright (c) 2017 Jan Tuck.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software/plugin and associated documentation files (the "Software/Plugin"), to deal in the Software/Plugin without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software/Plugin.
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software/Plugin.
 *
 * THE SOFTWARE/PLUGIN IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE/PLUGIN OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package me.jantuck.spawnshop.events;

import me.jantuck.spawnshop.SSCore;
import me.jantuck.spawnshop.configmanager.ConfigManager;
import me.jantuck.spawnshop.mobtypes.MobType;
import me.jantuck.spawnshop.utils.InventoryUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Jan on 27-01-2017.
 */
public class BlockPlaceEvent implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void blockPlaceEvent(org.bukkit.event.block.BlockPlaceEvent event) {
        if (event.isCancelled()) return;
        Player player = event.getPlayer();
        ItemStack itemStack = event.getItemInHand();
        if (itemStack != null && itemStack.hasItemMeta() && itemStack.getItemMeta().hasDisplayName() && itemStack.getType() == Material.MOB_SPAWNER) {
            String displayName = itemStack.getItemMeta().getDisplayName();
            if (displayName.startsWith(ConfigManager.SPAWNER_PREFIX)) {
                String itemName = " " + ChatColor.stripColor(displayName);
                MobType mobType = MobType.matchMobType(itemName);
                CustomPlayerPlaceSpawnerEvent spawnerPlaceEvent = new CustomPlayerPlaceSpawnerEvent(player, mobType);
                Bukkit.getPluginManager().callEvent(spawnerPlaceEvent);
                if (spawnerPlaceEvent.isCancelled()) {
                    return;
                }
                if (SSCore.getSSCore().hasSilkSpawners()) {
                    event.setCancelled(true);
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            mobType.setSpawner(event.getBlockPlaced());
                            InventoryUtil.removeItem(player, 1, mobType.getSpawner());
                        }
                    }.runTaskLater(SSCore.getSSCore(), (long) 0.4);
                } else {
                    mobType.setSpawner(event.getBlockPlaced());
                }
            }
        }
    }
}
