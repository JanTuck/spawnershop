/*
 * Copyright (c) 2017 Jan Tuck.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software/plugin and associated documentation files (the "Software/Plugin"), to deal in the Software/Plugin without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software/Plugin.
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software/Plugin.
 *
 * THE SOFTWARE/PLUGIN IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE/PLUGIN OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package me.jantuck.spawnshop.events;

import me.jantuck.spawnshop.configmanager.ConfigManager;
import me.jantuck.spawnshop.mobtypes.MobType;
import me.jantuck.spawnshop.utils.InventoryUtil;
import me.jantuck.spawnshop.utils.StringUtils;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.EquipmentSlot;

import static me.jantuck.spawnshop.SSCore.econ;

/**
 * Created by Jan on 28-01-2017.
 */
public class PlayerInteractEvent implements Listener {


    @EventHandler
    public void onPlayerInteractEvent(org.bukkit.event.player.PlayerInteractEvent event) {
        if (!(event.getAction() == Action.RIGHT_CLICK_BLOCK) && event.getHand() == EquipmentSlot.HAND) return;
        if (event.getClickedBlock().getState() instanceof Sign) {
            Sign sign = (Sign) event.getClickedBlock().getState();
            Player player = event.getPlayer();
            if (sign.getLine(0).equals(ConfigManager.getConfigManager().getSellSignMessage())) {
                if (!player.hasPermission("spawners.sign.sell")) {
                    player.sendMessage("&cYou do not have permission to use the buy spawner sign.");
                    return;
                }
                int price = StringUtils.toInteger(sign.getLine(3).replaceAll("[^0-9]", ""));
                if (price > 0) {
                    MobType mobType = MobType.matchMobType(ChatColor.stripColor(sign.getLine(2)));
                    if (!mobType.isEnabled()) {
                        sign.setLine(0, ChatColor.DARK_BLUE + "Invalid Mob Type");
                        sign.setLine(2, "Not Enabled!");
                        sign.setLine(1, "");
                        sign.setLine(3, "");
                        sign.update();
                        return;
                    }
                    int amount = StringUtils.toInteger(sign.getLine(1));
                    if (!InventoryUtil.removeItem(player, amount, mobType.getSpawner())) {
                        player.sendMessage(ConfigManager.getConfigManager().getNotEnoughItemsMessage().replace("%type%", mobType.getName()).replace("%amount%", amount + ""));
                    } else {
                        player.sendMessage(ConfigManager.getConfigManager().getSellSuccessMessage().replace("%type%", mobType.getName()).replace("%money%", sign.getLine(3)).replace("%amount%", amount + ""));
                        econ.depositPlayer(player, price);
                    }
                }
            } else if (sign.getLine(0).equals(ConfigManager.getConfigManager().getBuySignMessage())) {
                if (!player.hasPermission("spawners.sign.buy")) {
                    player.sendMessage("&cYou do not have permission to use the buy spawner sign.");
                    return;
                }
                int price = StringUtils.toInteger(sign.getLine(3).replaceAll("[^0-9]", ""));
                if (price > 0) {
                    MobType mobType = MobType.matchMobType(ChatColor.stripColor(sign.getLine(2)));
                    if (!mobType.isEnabled()) {
                        sign.setLine(0, ChatColor.DARK_BLUE + "Invalid Mob Type");
                        sign.setLine(2, "Not Enabled!");
                        sign.setLine(1, "");
                        sign.setLine(3, "");
                        sign.update();
                        return;
                    }
                    EconomyResponse response = econ.withdrawPlayer(player, price);
                    if (response.transactionSuccess()) {


                        int amount = StringUtils.toInteger(sign.getLine(1));
                        for (int i = 0; i < amount; i++) {
                            int firstEmpty = player.getInventory().firstEmpty();

                            if (firstEmpty == -1) {
                                player.getWorld().dropItem(player.getLocation().add(0, 0.5, 0), mobType.getSpawner());
                            } else {
                                player.getInventory().addItem(mobType.getSpawner());
                            }
                        }
                        player.sendMessage(ConfigManager.getConfigManager().getBuyMessage().replace("%type%", mobType.getName()).replace("%money%", sign.getLine(3)).replace("%amount%", amount + ""));
                    } else {
                        player.sendMessage(ConfigManager.getConfigManager().getNotEnoughMoneyMessage().replace("%type%", sign.getLine(2)));

                    }
                }

            }
        }
    }
}
