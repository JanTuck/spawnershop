package me.jantuck.spawnshop.utils;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

/**
 * Created by Jan on 30-01-2017.
 */
public class InventoryUtil {

    public static boolean removeItem(Player player, int amount, ItemStack itemStack) {
        PlayerInventory inventory = player.getInventory();
        if (inventory.containsAtLeast(itemStack, amount)) {
            itemStack.setAmount(amount);
            inventory.removeItem(itemStack);
            return true;
        } else {
            return false;
        }
    }

}
