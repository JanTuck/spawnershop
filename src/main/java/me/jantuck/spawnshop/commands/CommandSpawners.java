/*
 * Copyright (c) 2017 Jan Tuck.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software/plugin and associated documentation files (the "Software/Plugin"), to deal in the Software/Plugin without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software/Plugin.
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software/Plugin.
 *
 * THE SOFTWARE/PLUGIN IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE/PLUGIN OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package me.jantuck.spawnshop.commands;

import me.jantuck.spawnshop.configmanager.ConfigManager;
import me.jantuck.spawnshop.dynamiccommands.DynamicCommand;
import me.jantuck.spawnshop.mobtypes.MobType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

/**
 * Created by Jan on 27-01-2017.
 */
public class CommandSpawners extends DynamicCommand {

    public CommandSpawners() {
        super(ConfigManager.getConfigManager().getCommand(), "Open spawner gui.", "/" + ConfigManager.getConfigManager().getCommand(), "spawner.gui", ChatColor.RED + "You do not have permission to open the spawner gui.");
    }

    @Override
    public boolean execute(CommandSender commandSender, Command command, String commandLabel, String[] args) {
        if (!(commandSender instanceof Player)) return false;
        if (args.length == 1) {
            String arg = args[0];
            if (arg.equals("reload")) {
                if (!commandSender.hasPermission("spawner.reload")) {
                    commandSender.sendMessage(ChatColor.RED + "&c You do not have permission to reload the spawners config.");
                    return true;
                }
                ConfigManager.getConfigManager().reloadConfig();
                commandSender.sendMessage(ChatColor.GREEN + "Reloaded config!");
            } else {
                commandSender.sendMessage(ChatColor.RED + "Invalid args!");
            }
        } else {
            Player player = (Player) commandSender;
            Inventory inventory = Bukkit.createInventory(null, 45, ChatColor.RED + "" + ChatColor.BOLD + "Spawner Shop");
            for (MobType mobType : MobType.getAllMobTypes()) {
                if (mobType.isEnabled()) {
                    mobType.updateLoreAndPrice();
                    inventory.addItem(mobType.getSpawnEgg());
                }
            }
            player.openInventory(inventory);
        }
        return true;
    }
}
