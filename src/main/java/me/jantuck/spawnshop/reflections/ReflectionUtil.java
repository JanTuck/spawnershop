/*
 * Copyright (c) 2017 Jan Tuck.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software/plugin and associated documentation files (the "Software/Plugin"), to deal in the Software/Plugin without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software/Plugin.
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software/Plugin.
 *
 * THE SOFTWARE/PLUGIN IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE/PLUGIN OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package me.jantuck.spawnshop.reflections;

import org.bukkit.Bukkit;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jan on 1/20/2017.
 */
@SuppressWarnings({"DefaultFileTemplate", "WeakerAccess"})
public class ReflectionUtil {

    public static final String VERSION = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
    public static final Class<?> CRAFT_PLAYER_CLASS = ReflectionUtil.getCBClass("entity.CraftPlayer");
    public static final Class<?> PACKET_CLASS = getNMSClass("Packet");
    public static final Class<?> ICHAT_BASE_COMPONENT_CLASS = getNMSClass("IChatBaseComponent");
    public static final Class<?> ICHAT_BASE_COMPONENT__SERIALIZER_CLASS = ICHAT_BASE_COMPONENT_CLASS.getDeclaredClasses()[0];

    public static final Class<?> CRAFT_SERVER = ReflectionUtil.getCBClass("CraftServer");
    public static final Class<?> MINECRAFT_SERVER = ReflectionUtil.getNMSClass("MinecraftServer");
    public static final Class<?> PLAYER_LIST = ReflectionUtil.getNMSClass("PlayerList");
    /*
    public static final Class<?> PACKET_PLAY_OUT_BLOCK_BREAK_ANIMATION = ReflectionUtil.getNMSClass("PacketPlayOutBlockBreakAnimation");
    public static final Class<?> PACKET_PLAY_OUT_BLOCK_ACTION = ReflectionUtil.getNMSClass("PacketPlayOutBlockAction");
    public static final Class<?> PACKET_PLAY_OUT_BLOCK_CHANGE = ReflectionUtil.getNMSClass("PacketPlayOutBlockChange");
    public static final Class<?> PACKET_PLAY_OUT_BOSS = ReflectionUtil.getNMSClass("PacketPlayOutBoss");
    public static final Class<?> PACKET_PLAY_OUT_KICK_DISCONNECT = ReflectionUtil.getNMSClass("PacketPlayOutKickDisconnect");
    public static final Class<?> PACKET_PLAY_OUT_EXPLOSION = ReflectionUtil.getNMSClass("PacketPlayOutExplosion");
    public static final Class<?> PACKET_PLAY_OUT_UNLOAD_CHUNK = ReflectionUtil.getNMSClass("PacketPlayOutUnloadChunk");
    public static final Class<?> PACKET_PLAY_OUT_NAMED_SOUND_EFFECT = ReflectionUtil.getNMSClass("PacketPlayOutNamedSoundEffect");
    public static final Class<?> PACKET_PLAY_OUT_WORLD_PARTICLES = ReflectionUtil.getNMSClass("PacketPlayOutWorldParticles");
    public static final Class<?> PACKET_PLAY_OUT_OPEN_SIGN_EDITOR = ReflectionUtil.getNMSClass("PacketPlayOutOpenSignEditor");
    public static final Class<?> PACKET_PLAY_OUT_BED = ReflectionUtil.getNMSClass("PacketPlayOutBed");
    public static final Class<?> PACKET_PLAY_OUT_ENTITY_DESTROY = ReflectionUtil.getNMSClass("PacketPlayOutEntityDestroy");
    public static final Class<?> PACKET_PLAY_OUT_RESOURCE_PACK_SEND = ReflectionUtil.getNMSClass("PacketPlayOutResourcePackSend");
    public static final Class<?> PACKET_PLAY_OUT_RESPAWN = ReflectionUtil.getNMSClass("PacketPlayOutRespawn");
    public static final Class<?> PACKET_PLAY_OUT_WORLD_BORDER = ReflectionUtil.getNMSClass("PacketPlayOutWorldBorder");
    public static final Class<?> PACKET_PLAY_OUT_CAMERA = ReflectionUtil.getNMSClass("PacketPlayOutCamera");
    public static final Class<?> PACKET_PLAY_OUT_HELD_ITEM_SLOT = ReflectionUtil.getNMSClass("PacketPlayOutHeldItemSlot");
    public static final Class<?> PACKET_PLAY_OUT_PLAYER_INFO = ReflectionUtil.getNMSClass("PacketPlayOutPlayerInfo");
    public static final Class<?> PACKET_PLAY_OUT_NAMED_ENTITY_SPAWN = ReflectionUtil.getNMSClass("PacketPlayOutNamedEntitySpawn");


    public static final Class<?> ENUM_DIFFICULTY = ReflectionUtil.getNMSClass("EnumDifficulty");
    public static final Class<?> WORLD_TYPE = getNMSClass("WorldType");
    public static final Class<?> ENUM_GAMEMODE = getNMSClass("EnumGamemode");
    */

    private static Map<String, Class> builtInMap = new HashMap<>();
    private static Map<String, Class> arrays = new HashMap<>();

    static {
        builtInMap.put("integer", Integer.TYPE);
        builtInMap.put("long", Long.TYPE);
        builtInMap.put("double", Double.TYPE);
        builtInMap.put("float", Float.TYPE);
        builtInMap.put("boolean", Boolean.TYPE);
        builtInMap.put("character", Character.TYPE);
        builtInMap.put("byte", Byte.TYPE);
        builtInMap.put("void", Void.TYPE);
        builtInMap.put("short", Short.TYPE);
    }

    public static <T> T getPrivateFieldObject(Class<?> methodClass, Object handle, String fieldName) {
        try {
            Field field = methodClass.getDeclaredField(fieldName);
            field.setAccessible(true);
            return (T) field.get(handle);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T getFieldObject(Class<?> methodClass, Object handle, String fieldName) {
        try {
            return (T) methodClass.getField(fieldName).get(handle);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Class<?> getCBClass(String name) {
        try {
            return Class.forName("org.bukkit.craftbukkit." + VERSION + "." + name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Method getMethod(Class<?> methodClass, String method, Class<?>... classParams) {
        try {
            return methodClass.getMethod(method, classParams);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean invokeMethod(Method method, Object handle, Object... parameters) {
        try {
            method.invoke(handle, parameters);
            return true;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Field getField(Class<?> methodClass, String fieldName) {
        try {
            return methodClass.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean setAccessible(Field field, boolean value) {
        field.setAccessible(value);
        return true;
    }

    public static boolean setField(Field field, Object handle, Object value) {
        try {
            field.set(handle, value);
            return true;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean invokeMethod(Class<?> methodClass, Object handle, String method, Object... parameters) {
        Class<?>[] classes = getCorrectClassParameters(methodClass, method, false, parameters);
        try {
            methodClass.getMethod(method, classes).invoke(handle, parameters);
            return true;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return false;
    }

    private static Class<?>[] getCorrectClassParameters(Class<?> superClass, String method, boolean constructor, Object... parameters) {
        Class<?>[] classes = new Class[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            if (builtInMap.containsKey(parameters[i].getClass().getSimpleName().toLowerCase())) {
                classes[i] = builtInMap.get(parameters[i].getClass().getSimpleName().toLowerCase());
            } else {
                classes[i] = parameters[i].getClass();
            }
        }
        if (constructor) {
            if (hasConstructor(superClass, classes)) {
                return classes;
            }
        } else {
            if (hasMethod(superClass, method, classes)) {
                return classes;
            }
        }
        return getCleanClassParameters(parameters);
    }

    private static boolean hasConstructor(Class<?> methodClass, Class<?>... parameters) {
        try {
            return (methodClass.getConstructor(parameters) instanceof Constructor);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return false;
    }

    private static boolean hasMethod(Class<?> methodClass, String method, Class<?>... parameters) {
        try {
            return (methodClass.getMethod(method, parameters) instanceof Method);
        } catch (NoSuchMethodException e) {
            return false;
        }
    }

    private static Class<?>[] getCleanClassParameters(Object... parameters) {
        Class<?>[] classes = new Class[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            classes[i] = parameters[i].getClass();
        }
        return classes;
    }

    public static <T> T invokeMethodReturnObject(Class<?> methodClass, Object handle, String method, Object... parameters) {
        Class[] classes = getCorrectClassParameters(methodClass, method, false, parameters);
        try {
            return (T) methodClass.getMethod(method, classes).invoke(handle, parameters);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T getObjectFromConstructor(Class<?> methodClass, Object... parameters) {
        Class[] classes = getCorrectClassParameters(methodClass, null, true, parameters);
        try {
            return (T) methodClass.getConstructor(classes).newInstance(parameters);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T getObjectFromConstructor(Constructor<?> constructor, Object... parameters) {

        try {
            return (T) constructor.newInstance(parameters);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Constructor<?> getConstructor(Class<?> methodClass, Class<?>... parameters) {
        try {
            return methodClass.getConstructor(parameters);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Constructor<?> getConstructor(Class<?> superClass) {
        try {
            return superClass.getConstructor();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Class<?> getNMSClass(String name) {
        try {
            return Class.forName("net.minecraft.server." + VERSION + "." + name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


}
